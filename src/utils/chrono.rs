use std::time::Instant;

lazy_static::lazy_static! {
    static ref CHRONOMETER: Instant = Instant::now(); 
}

pub fn elapsed_ms() -> u64 {
    CHRONOMETER.elapsed().as_millis() as u64
}
