
const A4_FREQUENCY: f32 = 440.0;
const A4_ID: usize = 33;
const A4_MAX_ID: usize = 57;
const C_MIN_ID: usize = (A4_ID + 3) % 12;

pub fn get_note_name(note_id: usize) -> &'static str {
    let shift = 12 - (A4_ID % 12);
    match (note_id + shift) % 12 {
        0  => "A",
        1  => "A#",
        2  => "B",
        3  => "C",
        4  => "C#",
        5  => "D",
        6  => "D#",
        7  => "E",
        8  => "F",
        9  => "F#",
        10 => "G",
        11 => "G#",
        _  => unreachable!(),
    }
}

pub fn get_octave_name(note_id: usize) -> &'static str {
    match (note_id + A4_MAX_ID - A4_ID) / 12 {
        0 => "0",
        1 => "1",
        2 => "2",
        3 => "3",
        4 => "4",
        5 => "5",
        6 => "6",
        7 => "7",
        8 => "8",
        _ => "-"
    }
}

pub fn get_c_next_octave(note_id: usize) -> usize {
    ((note_id + 12 - C_MIN_ID) / 12) * 12 + C_MIN_ID 
}

pub fn get_note_frequency(note_id: usize) -> f32 {
    A4_FREQUENCY * 2f32.powf((note_id as f32 - A4_ID as f32) / 12.0)
}

pub fn get_obertone_normalizer(obertones: &[(f32,f32)]) -> f32 {
    obertones.iter().map(|ob| ob.1).sum::<f32>() + 1f32
}